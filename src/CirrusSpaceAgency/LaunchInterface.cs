using System;
using System.Threading;
using CirrusSpaceAgency.Models;

namespace CirrusSpaceAgency
{
    public class LaunchInterface
    {
        public void ShowSplash()
        {
            Console.WriteLine("**************************************************************");
            Console.WriteLine("        !                                                    ");
            Console.WriteLine("        !                                                    ");
            Console.WriteLine("        ^                                                    ");
            Console.WriteLine("       / \\                                                  ");
            Console.WriteLine("      /___\\                Cirrus Space Agency              ");
            Console.WriteLine("     |=   =|               Launch Control System             ");
            Console.WriteLine("     |     |                    Version 1.0                  ");
            Console.WriteLine("     |     |                                                 ");
            Console.WriteLine("     |     |                                                 ");
            Console.WriteLine("     |     |                                                 ");
            Console.WriteLine("     |     |                                                 ");
            Console.WriteLine("     |     |                                                 ");
            Console.WriteLine("     |     |                                                 ");
            Console.WriteLine("     |     |                                                 ");
            Console.WriteLine("     |     |                                                 ");
            Console.WriteLine("    /|##!##|\\                                               ");
            Console.WriteLine("   / |##!##| \\                                              ");
            Console.WriteLine("  /  |##!##|  \\                                             ");
            Console.WriteLine(" |  / ^ | ^ \\  |                                            ");
            Console.WriteLine(" | /  ( | )  \\ |                                            ");
            Console.WriteLine(" |/   ( | )   \\|                                            ");
            Console.WriteLine("     ((   ))                                                 ");
            Console.WriteLine("    ((  :  ))                                                ");
            Console.WriteLine("    ((  :  ))                                                ");
            Console.WriteLine("     ((   ))                                                 ");
            Console.WriteLine("      (( ))                                                  ");
            Console.WriteLine("       ( )                                                   ");
            Console.WriteLine("        .                                                    ");
            Console.WriteLine("        .                                                    ");
            Console.WriteLine("        .                                                    ");
            Console.WriteLine("**************************************************************");
        }

        public OrbitalParameters PromptForLaunchParameters()
        {
            Console.WriteLine("Enter target orbital parameters? (y/n):");
            var answer = Console.ReadLine();
            if (answer.ToLower() != "y" || answer.ToLower() != "yes") {
                Console.WriteLine("Default orbital parameters selected, continuing with launch procedures...");
                return new OrbitalParameters();
            }

            var orbitalParameters = new OrbitalParameters();

            Console.WriteLine("Target Apogee (default is 80,010m):");
            answer = Console.ReadLine();
            orbitalParameters.TargetApogee = answer.Length > 0 ? float.Parse(answer) : orbitalParameters.TargetApogee;

            Console.WriteLine("Target Perigee (default is 80,000m):");
            answer = Console.ReadLine();
            orbitalParameters.TargetPerigee = answer.Length > 0 ? float.Parse(answer) : orbitalParameters.TargetPerigee;

            Console.WriteLine("Turn Start Altitude (default is 250m):");
            answer = Console.ReadLine();
            orbitalParameters.TurnStartAltitude = answer.Length > 0 ? float.Parse(answer) : orbitalParameters.TurnStartAltitude;

            Console.WriteLine("Turn End Altitude (default is 50000m):");
            answer = Console.ReadLine();
            orbitalParameters.TurnEndAltitude = answer.Length > 0 ? float.Parse(answer) : orbitalParameters.TurnEndAltitude;

            Console.WriteLine("Heading (default is 90 degrees):");
            answer = Console.ReadLine();
            orbitalParameters.Heading = answer.Length > 0 ? int.Parse(answer) : orbitalParameters.Heading;

            return orbitalParameters;
        }

        public void Countdown()
        {
            Console.WriteLine("We have a go for launch, rocket's computer has taken over the countdown");
            Thread.Sleep(1000);
            Console.WriteLine ("3...");
            System.Threading.Thread.Sleep (1000);
            Console.WriteLine ("2...");
            System.Threading.Thread.Sleep (1000);
            Console.WriteLine ("1...");
            System.Threading.Thread.Sleep (1000);
            Console.WriteLine ("Launch!");
        }
    }
}