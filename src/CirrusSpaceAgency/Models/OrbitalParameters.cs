namespace CirrusSpaceAgency.Models
{
    public class OrbitalParameters
    {
        public OrbitalParameters()
        {
            TurnStartAltitude = 250;
            TurnEndAltitude = 50000;
            TargetApogee = 80010;
            TargetPerigee = 80000;
            Heading = 90;
        }

        public float TurnStartAltitude { get; set; }
        public float TurnEndAltitude { get; set; }
        public float TargetApogee { get; set; }
        public float TargetPerigee { get; set; }
        public float Heading { get; set; }

        public bool IsValid() {
            return TurnEndAltitude > TurnStartAltitude
                && TargetApogee > TargetPerigee
                && Heading >= 0;
        }

        public string GetErrorMsg() {
            if (TurnEndAltitude <= TurnStartAltitude) return "The turn altitude end must be more than the start.";
            if (TargetApogee <= TargetPerigee) return "Apogee must be higher than perigee.";
            if (Heading < 0) return "Heading must be between 0 and 360.";
            return null;
        }
    }
}