using System.Linq;
using KRPC.Client.Services.SpaceCenter;
using CirrusSpaceAgency.Rockets;

namespace CirrusSpaceAgency.Rockets.PhoenixSeries
{
    public class PhoenixA0 : Rocket
    {
        public PhoenixA0(Vessel vessel) : base(vessel)
        {
        }

        public override int FirstStageIndex => 2;

        public override bool ShouldItStage()
        {
            switch (this.CurrentStageIndex)
            {
                // First stage, so check the active liquid fuel engines
                case 2:
                    var activeLiquidEngines = this.Vessel.Parts.Engines.Where(e => e.Active && e.CanRestart);
                    return activeLiquidEngines.All(e => e.Thrust == 0);

                // This is just a decouple stage that will trigger Sepatrons on the first stage
                case 1: return true;

                default: return false;
            }
        }
    }
}