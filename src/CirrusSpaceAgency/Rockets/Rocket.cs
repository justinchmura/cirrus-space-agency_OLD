using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;

namespace CirrusSpaceAgency.Rockets
{
    public abstract class Rocket
    {
        private Connection _connection;

        public Vessel Vessel;
        public int CurrentStageIndex { get; private set; }
        public int CurrentDecoupleStageIndex => this.CurrentStageIndex - 1;
        public bool CurrentStageCanThrottle { get; private set; }
        public IList<Fairing> Fairings { get; private set; }

        public Stream<double> CurrentSpeed { get; private set; }
        public Stream<Tuple<double, double, double>> Velocity { get; private set; }
        public Stream<double> CurrentApogee { get; private set; }
        public Stream<float> CurrentAngleOfAttack { get; private set; }
        public Stream<float> CurrentPitch { get; private set; }
        public Stream<float> CurrentThrust { get; private set; }
        public Stream<double> CurrentSurfaceAltitude { get; private set; }
        public Stream<float> DynamicPressure { get; private set; }

        public abstract int FirstStageIndex { get; }

        public Rocket(Vessel vessel)
        {
            this.Vessel = vessel;
            this.CurrentStageIndex = this.FirstStageIndex;
            this.Fairings = this.Vessel.Parts.Fairings;
        }

        public abstract bool ShouldItStage();

        public void InitiateVehicleTelemetry(Connection connection)
        {
            this._connection = connection;

            var surfaceVelocityFrame = this.Vessel.SurfaceVelocityReferenceFrame;
            var surfaceSpeedFrame = this.Vessel.Orbit.Body.ReferenceFrame;
            var stageResources = this.Vessel.ResourcesInDecoupleStage(this.CurrentStageIndex - 1, false);
            this.CurrentSpeed = connection.AddStream(() => this.Vessel.Flight(surfaceSpeedFrame).Speed);
            this.Velocity = connection.AddStream(() => this.Vessel.Flight(surfaceVelocityFrame).Velocity);
            this.CurrentApogee = connection.AddStream(() => this.Vessel.Orbit.ApoapsisAltitude);
            this.CurrentPitch = connection.AddStream(() => this.Vessel.AutoPilot.TargetPitch);
            this.CurrentThrust = connection.AddStream(() => this.Vessel.Thrust);
            this.CurrentAngleOfAttack = connection.AddStream(() => this.Vessel.Flight(surfaceVelocityFrame).AngleOfAttack);
            this.CurrentSurfaceAltitude = connection.AddStream(() => this.Vessel.Flight(surfaceVelocityFrame).SurfaceAltitude);
            this.DynamicPressure = connection.AddStream(() => this.Vessel.Flight(surfaceVelocityFrame).DynamicPressure);
        }

        public void ReadyVehicleForLaunch()
        {
            this.Vessel.AutoPilot.Engage();
            this.Vessel.Control.Throttle = 1;
            this.Vessel.AutoPilot.TargetPitchAndHeading(90, 90);
        }

        public void ActivateNextStage(bool isLaunch)
        {
            this.Vessel.Control.ActivateNextStage();
            if (this.CurrentStageIndex > 0 && !isLaunch) this.CurrentStageIndex -= 1;
            Thread.Sleep(250);
            this.CheckIfStageCanThrottle();
        }

        public void PerformRollProgram()
        {
            Console.WriteLine("Go for roll program...");
            this.Vessel.AutoPilot.TargetRoll = 0;
            this.Vessel.AutoPilot.Wait();
        }

        public void JettisonFairing()
        {
            foreach (var fairing in this.Fairings) fairing.Jettison();
        }

        private void CheckIfStageCanThrottle()
        {
            this.CurrentStageCanThrottle = this.Vessel.Parts
                .InStage(this.CurrentStageIndex)
                .Where(p => p.Engine != null && p.Engine.Active)
                .Select(p => p.Engine)
                .Any(e => !e.ThrottleLocked);
            Console.WriteLine($"Can Stage Throttle: {this.CurrentStageCanThrottle}");
        }
    }
}