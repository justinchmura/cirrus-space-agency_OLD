using System;
using System.Timers;
using System.Linq;
using System.Threading;
using KRPC.Client;
using KRPC.Client.Services.KRPC;
using KRPC.Client.Services.SpaceCenter;
using CirrusSpaceAgency.Models;
using CirrusSpaceAgency.Rockets;

namespace CirrusSpaceAgency
{
    public class LaunchComputer
    {
        private OrbitalParameters _orbitalParameters;

        public Connection connection { get; }

        public LaunchComputer(Connection _krpcConnection)
        {
            this.connection = _krpcConnection;
        }

        public void FeedLaunchParameters(OrbitalParameters orbitalParameters)
        {
            this._orbitalParameters = orbitalParameters;
        }

        public void Launch(Rocket rocket)
        {
            // Create 5 second launch timer for read outs
            var launchTimer = new System.Timers.Timer();
            launchTimer.Interval = 5000;
            launchTimer.Elapsed += new ElapsedEventHandler((src, e) => {
                // Console.WriteLine($"Altitue: {altitude.Get()} -- Speed: {speed.Get()}");
            });
            launchTimer.Enabled = true;

            this.Takeoff(rocket, 85);
            this.GravityTurn(rocket);
        }

        private void Takeoff(Rocket rocket, float turnStartSpeed)
        {
            rocket.ActivateNextStage(true);
            Thread.Sleep(5000);
            rocket.PerformRollProgram();

            while (rocket.CurrentSpeed.Get() < turnStartSpeed) Thread.Sleep(500);
            rocket.Vessel.AutoPilot.TargetHeading = this._orbitalParameters.Heading;
        }

        private void GravityTurn(Rocket rocket)
        {
            Console.WriteLine("Starting gravity turn...");


            while (true)
            {
                this.CheckStaging(rocket);

                // If we're below 10km, just make sure we don't get going too fast so when approaching 300 m/s,
                // cut the throttle down to 70%.
                if (rocket.CurrentSurfaceAltitude.Get() < 8000)
                {
                    if (rocket.CurrentSpeed.Get() < 300)
                    {
                        rocket.Vessel.Control.Throttle = 1f;
                    }
                    else
                    {
                        while (rocket.Vessel.Control.Throttle > 0.7)
                        {
                            rocket.Vessel.Control.Throttle -= 0.05f;
                            Thread.Sleep(100);
                        }
                    }
                }

                // Make sure to maintain a good pitch to acheive the designated apogee
                var belowTarget = rocket.CurrentApogee.Get() < this._orbitalParameters.TargetApogee;
                if (belowTarget)
                {
                    this.PitchForApogee(rocket);
                }

                // Go into overshoot procedures if we somehow get over the designated apogee
                if (rocket.CurrentApogee.Get() > this._orbitalParameters.TargetApogee)
                {
                    this.OvershootApogee(rocket);
                }

                // TODO: ADD LOGIC FOR SECONDARY ORBITAL MANEUVER AND BURN

                // When dynamic pressure is below 10, it's safe to eject the fairing without damaging
                // the payload
                var goForFairingJettison = rocket.DynamicPressure.Get() < 10;
                if (goForFairingJettison) rocket.JettisonFairing();

                Thread.Sleep(500);
            }
        }

        private void PitchForApogee(Rocket rocket)
        {
            var aoa = rocket.CurrentAngleOfAttack.Get();
            if (5 > aoa && aoa > -5 && this.IsApogeeIncreasing(rocket))
            {
                var newPitch = rocket.CurrentPitch.Get() - 3;
                Thread.Sleep(300);
                rocket.Vessel.AutoPilot.TargetPitch = newPitch;
                rocket.Vessel.AutoPilot.Wait();
            }
            if (this.IsApogeeDecreasing(rocket))
            {
                var newPitch = rocket.CurrentPitch.Get() + 3;
                Thread.Sleep(300);
                rocket.Vessel.AutoPilot.TargetPitch = newPitch;
                rocket.Vessel.AutoPilot.Wait();
            }
        }

        private void OvershootApogee(Rocket rocket)
        {
            var aoa = rocket.CurrentAngleOfAttack.Get();
            if (5 > aoa && aoa > -5)
            {
                var newPitch = rocket.CurrentPitch.Get() - 3;
                Thread.Sleep(100);
                rocket.Vessel.AutoPilot.TargetPitch = newPitch;
            }
            Thread.Sleep(200);
        }

        private bool IsApogeeIncreasing(Rocket rocket)
        {
            var cachedApogee = rocket.CurrentApogee.Get();
            Thread.Sleep(100);
            return cachedApogee - rocket.CurrentApogee.Get() >= 0;
        }

        private bool IsApogeeDecreasing(Rocket rocket)
        {
            return !IsApogeeIncreasing(rocket);
        }

        private void CheckStaging(Rocket rocket)
        {
            if (rocket.ShouldItStage())
            {
                rocket.ActivateNextStage(false);
            }
        }
    }
}