﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Timers;
using KRPC.Client;
using KRPC.Client.Services.KRPC;
using KRPC.Client.Services.SpaceCenter;
using CirrusSpaceAgency.Rockets;
using CirrusSpaceAgency.Rockets.PhoenixSeries;

namespace CirrusSpaceAgency
{
    class Program
    {
        static void Main(string[] args)
        {
            // ----------------------------------------------------------------
            // Connection Options
            // ----------------------------------------------------------------
            var name = "Cirrus Space Agency -- Launch Control Center";
            var address = IPAddress.Parse("127.0.0.1");
            var rpcPort = 50000;
            var streamPort = 50001;
            // ----------------------------------------------------------------

            var launchInterface = new LaunchInterface();
            launchInterface.ShowSplash();
            Thread.Sleep(1000);

            // ----------------------------------------------------------------
            // Connect to kRPC
            // ----------------------------------------------------------------
            using (var connection = new Connection(name, address, rpcPort, streamPort))
            {

                // This should be replaced at some point by something that the lifter
                // can be selected either through a UI or CMD line parameter.
                var rocket = new PhoenixA0(connection.SpaceCenter().ActiveVessel);

                rocket.InitiateVehicleTelemetry(connection);

                var orbitalParameters = launchInterface.PromptForLaunchParameters();

                var launchComputer = new LaunchComputer(connection);
                launchComputer.FeedLaunchParameters(orbitalParameters);

                rocket.ReadyVehicleForLaunch();
                
                launchInterface.Countdown();

                launchComputer.Launch(rocket);
            }
        }
    }
}
